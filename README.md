# sentinel-nacos

#### 介绍
spring cloud 与 sentinel集成服务限流、降级、熔断。
sentinel与nacos集成做持久化存储。

<br />
sentinel-dashboard ： sentinel控制台为1.7.2版本，与nacos持久化存储编译好的工程
<br />
springboot_provider ：提供远程服务业务接口
<br />
springboot_sentinel ：演示，服务降级、限流、熔断功能
<br />

#### 特技

1.  使用 Readme\_XXX.md 来支持不同的语言，例如 Readme\_en.md, Readme\_zh.md
2.  Gitee 官方博客 [blog.gitee.com](https://blog.gitee.com)
3.  你可以 [https://gitee.com/explore](https://gitee.com/explore) 这个地址来了解 Gitee 上的优秀开源项目
4.  [GVP](https://gitee.com/gvp) 全称是 Gitee 最有价值开源项目，是综合评定出的优秀开源项目
5.  Gitee 官方提供的使用手册 [https://gitee.com/help](https://gitee.com/help)
6.  Gitee 封面人物是一档用来展示 Gitee 会员风采的栏目 [https://gitee.com/gitee-stars/](https://gitee.com/gitee-stars/)
