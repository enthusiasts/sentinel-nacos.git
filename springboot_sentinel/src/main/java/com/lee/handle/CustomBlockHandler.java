package com.lee.handle;

import com.alibaba.csp.sentinel.slots.block.BlockException;

import java.util.HashMap;
import java.util.Map;

public class CustomBlockHandler {

    public static Map<String,Object> handleException(BlockException exception) {

        Map<String,Object> result = new HashMap<>();
        result.put("name","自定义限流信息");
        result.put("code",200);
        return result ;
    }

}
