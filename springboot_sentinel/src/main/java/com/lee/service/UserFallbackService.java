package com.lee.service;

import org.springframework.stereotype.Component;

import java.util.HashMap;
import java.util.Map;

@Component
public class UserFallbackService implements UserService {

    @Override
    public Map<String,Object> getInfo(Long id){

        Map<String,Object> result = new HashMap<>();
        result.put("name","service lower level");
        result.put("code",200);

        return result ;
    }
}
