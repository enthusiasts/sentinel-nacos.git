package com.lee.service;

import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;

import java.util.Map;

@FeignClient(value = "springboot-provider", fallback = UserFallbackService.class)
public interface UserService {

    @GetMapping("/user/{id}")
    Map<String,Object> getInfo(@PathVariable Long id);
}
