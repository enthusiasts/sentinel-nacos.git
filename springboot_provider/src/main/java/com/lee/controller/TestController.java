package com.lee.controller;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;

import java.util.HashMap;
import java.util.Map;

@RestController
public class TestController {


    @GetMapping("/user/{id}")
    public Map<String,Object> getInfo(@PathVariable(value = "id") String id) {

        if("1".equals(id)) {
            throw new RuntimeException("remote func is fail");
        }

        Map<String,Object> result = new HashMap<>();
        result.put("reqData",id);
        result.put("code","200");

        return result ;
    }
}
